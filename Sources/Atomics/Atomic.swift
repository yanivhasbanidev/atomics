//
//  AtomicProperty.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 25/07/2020.
//  Copyright © 2020 Yaniv Hasbani. All rights reserved.
//
import Foundation
import Combine

public final class Atomic<Value: DefualtedValue>: AtomicBase<Value> {
  public init(_ value: Value.ValueType = Value.defaultValue) {
    self._value = value
    
    super.init()
    
    self.state = .initialized
  }
  
  private var _value: Value.ValueType
  public var value: Value.ValueType {
    get {
      var value = Value.defaultValue
      self.queue.sync {
        value = self._value
      }
      
      return value
    }
  }
  
  /// Provides a closure that will be called synchronously. This closure will be passed in the current value
  /// and it is free to modify it. Any modifications will be saved back to the original value.
  /// No other reads/writes will be allowed between when the closure is called and it returns.
  public func mutate(_ closure: (inout Value.ValueType) -> Value.ValueType) {
    var value: Value.ValueType? = self.value
    self.state = .willSet(value as! Value?)
    
    self.queue.sync(flags: .barrier) {
      self._value = closure(&self._value)
      value = self._value
    }
    
    guard let value = value as? Value else {
      return
    }
    
    self.state = .didSet(value)
  }
  
  public func mutate(_ closure:() -> Value.ValueType) {
    var value: Value.ValueType? = self.value
    self.state = .willSet(value as! Value?)
    
    self.queue.sync(flags: .barrier) {
      self._value = closure()
      value = self._value
    }
    
    guard let value = value as? Value else {
      return
    }
    
    self.state = .didSet(value)
  }
}


