//
//  AtomicArray.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 02/04/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

public protocol AtomicArray: Sequence {
  associatedtype Element
  
  var first: Element? { get }
  var last: Element? { get }
  var isEmpty: Bool { get }

  func contains(where predicate: (Element) throws -> Bool) rethrows -> Bool
  
  func last(where predicate: (Element) throws -> Bool) rethrows -> Element?
  
  func shuffled() -> [Element]
  
  func first(where predicate: (Element) throws -> Bool) rethrows -> Element?
  
  func randomElement() -> Element?
  
  subscript(index: Int) -> Element { get }
  
  mutating func shuffle()
  mutating func reverse()
  mutating func append(_ newElement: Element)
  mutating func append<S>(contentsOf newElements: S) where Element == S.Element, S : Sequence
  
  mutating func popFirst() -> Element?
  mutating func popLast() -> Element?
  mutating func removeFirst() -> Element
  mutating func removeLast() -> Element
  mutating func removeFirst(_ k: Int)
  mutating func removeLast(_ k: Int)
  
  mutating func remove(at position: Int) -> Element
  mutating func removeSubrange(_ bounds: Range<Int>)

  mutating func removeAll(keepingCapacity keepCapacity: Bool)
  mutating func removeAll(where shouldBeRemoved: (Element) throws -> Bool) rethrows
}

public extension Atomic where Value.ValueType: AtomicArray {
  var isEmpty: Bool {
    return self.value.isEmpty
  }
  
  func randomElement() -> Value.ValueType.Element? {
    return self.value.randomElement()
  }
  
  func removeAll(where shouldBeRemoved: (Value.ValueType.Element) -> Bool) {
    self.mutate { wrappedValue in
      wrappedValue.removeAll(where: shouldBeRemoved)
      
      return wrappedValue
    }
  }
  
  func contains(where predicate: (Value.ValueType.Element) throws -> Bool) rethrows -> Bool {
    return try self.value.contains(where: predicate)
  }
  
  func append(_ newElement: Value.ValueType.Element) {
    self.mutate {
      $0.append(newElement)
      
      return $0
    }
  }
  
  func remove(at position: Int) {
    self.mutate {
      _ = $0.remove(at: position)
      
      return $0
    }
  }
  
  func first(where predicate: (Value.ValueType.Element) throws -> Bool) rethrows -> Value.ValueType.Element? {
    return try self.value.first(where: predicate)
  }
  
  subscript(index: Int) -> Value.ValueType.Element {
    return self.value[index]
  }
}

extension Array: AtomicArray & AtomicCollection {
  public mutating func popFirst() -> Element? {
    var slice = ArraySlice(self)
    
    let element = slice.popFirst()
    
    self = Array(slice)
    
    return element
  }
}
