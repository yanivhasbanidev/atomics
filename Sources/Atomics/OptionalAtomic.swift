//
//  OptionalAtomic.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 30/03/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

public final class OptionalAtomic<Value>: AtomicBase<Value> {
  public init(_ value: Value? = nil) {
    self._value = value
    
    super.init()
    
    self.state = .initialized
  }
  
  private var _value: Value?
  public var value: Value? {
    get {
      var value: Value?
      self.queue.sync {
        value = self._value
      }
      
      return value
    }
  }
  
  /// Provides a closure that will be called synchronously. This closure will be passed in the current value
  /// and it is free to modify it. Any modifications will be saved back to the original value.
  /// No other reads/writes will be allowed between when the closure is called and it returns.
  public func mutate(_ closure: (inout Value?) -> Value?) {
    self.state = .willSet(self.value)
    var value: Value?
    
    self.queue.sync(flags: .barrier) {
      self._value = closure(&self._value)
      value = self._value
    }
    
    self.state = .didSet(value)
  }
  
  public func mutate(_ closure: () -> Value?) {
    self.state = .willSet(self.value)
    var value: Value?
    
    self.queue.sync(flags: .barrier) {
      self._value = closure()
      value = self._value
    }
    
    self.state = .didSet(value)
  }
}
