//
//  AtomicBase.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 30/03/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

private let atomicityQueueTarget = DispatchQueue(label: "com.skektech.atomic.\(UUID())",
                                                 qos: .utility,
                                                 attributes: .concurrent)

public class AtomicBase<Value> {
  public enum State {
    case uninitialized
    case initialized
    case willSet(_ newValue: Value?)
    case didSet(_ newValue: Value?)
  }
  
  @Published public var state: Atomic.State = .uninitialized
  
  let queue = DispatchQueue(label: "com.skektech.atomic.\(UUID())",
                            attributes: .concurrent,
                            target: atomicityQueueTarget)
}
