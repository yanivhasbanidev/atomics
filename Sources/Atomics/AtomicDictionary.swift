//
//  AtomicDictinoary.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 02/04/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

public protocol AtomicDictionary {
  associatedtype Key: Hashable
  associatedtype Value
  
  typealias Element = (key: Key, value: Value)
  
  var count: Int { get }
  var isEmpty: Bool { get }
  
  var keys: Dictionary<Key, Value>.Keys { get }
  var values: Dictionary<Key, Value>.Values { get }
  
  var first: (key: Key, value: Value)? { get }
  func first(where predicate: (Element) throws -> Bool) rethrows -> Element?
  
  func dropFirst(_ k: Int) -> Slice<Dictionary<Key, Value>>
  func dropLast(_ k: Int) -> Slice<Dictionary<Key, Value>>
  func drop(while predicate: ((key: Key, value: Value)) throws -> Bool) rethrows -> Slice<Dictionary<Key, Value>>
  
  mutating func removeValue(forKey key: Key) -> Value?
  mutating func removeAll(keepingCapacity keepCapacity: Bool)
  
  mutating func updateValue(_ value: Value, forKey key: Key) -> Value?
  
  subscript(key: Key) -> Value? { get set }
  
  func enumerated() -> EnumeratedSequence<Dictionary<Key, Value>>
  func shuffled() -> [(key: Key, value: Value)]
  func reversed() -> [(key: Key, value: Value)]
}

public extension Atomic where Value.ValueType: AtomicDictionary {
  typealias Key = Value.ValueType.Key
  typealias DictionaryValue = Value.ValueType.Value
  
  var keys: [Key] {
    return Array(self.value.keys)
  }
  
  var values: Dictionary<Key, DictionaryValue>.Values {
    return self.value.values
  }
  
  func removeValue(forKey key: Key) {
    self.mutate {
      _ = $0.removeValue(forKey:key)
      
      return $0
    }
  }
  
  func updateValue(_ value: Value.ValueType.Value, forKey key: Key) {
    self.mutate {
      $0[key] = value
      
      return $0
    }
  }
  
  subscript(key: Key) -> Value.ValueType.Value? {
    get {
      return self.value[key]
    }
  }
}

extension Dictionary: AtomicDictionary & AtomicCollection where Key: Hashable {
  
}
