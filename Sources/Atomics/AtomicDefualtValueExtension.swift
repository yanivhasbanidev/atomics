//
//  AtomicDefualtValueExtension.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 30/03/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

public protocol DefualtedValue<ValueType> {
  associatedtype ValueType
  
  static var defaultValue: Self.ValueType { get }
  var isDefault: Bool { get }
}

extension Int: DefualtedValue {
  static public var defaultValue: Int { -Int.max }
  
  public var isDefault: Bool {
    self == Int.defaultValue
  }
}

extension Array: DefualtedValue {
  static public var defaultValue: Array { [] }
  
  public var isDefault: Bool {
    self.isEmpty
  }
}

extension Dictionary: DefualtedValue {
  
  public static var defaultValue: Dictionary { [:] }
  
  public var isDefault: Bool {
    self.isEmpty
  }
}

extension Bool: DefualtedValue {
  public static var defaultValue: Bool { false }
  
  public var isDefault: Bool {
    self == false
  }
}

extension Date: DefualtedValue {
  public static var defaultValue: Date { Date.distantFuture }
  
  public var isDefault: Bool {
    self == Date.distantFuture
  }
}

extension Data: DefualtedValue {
  public static var defaultValue: Data { Data() }
  
  public var isDefault: Bool {
    self.isEmpty
  }
}

extension Set: DefualtedValue {
  public static var defaultValue: Set { Set() }
  
  public var isDefault: Bool {
    self.isEmpty
  }
}
