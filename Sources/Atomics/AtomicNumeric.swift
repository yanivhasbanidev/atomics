//
//  AtomicNumeric.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 02/04/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

public extension Atomic where Value.ValueType == Int {
  static func == (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value == rhs
  }

  static func < (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value < rhs
  }
  
  static func > (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value > rhs
  }
  
  static func <= (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value <= rhs
  }

  static func >= (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value >= rhs
  }
  
  static func + (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value + rhs
  }
  
  static func - (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value - rhs
  }
  
  static func * (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value * rhs
  }
  
  static func / (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value / rhs
  }
  
  static func % (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value % rhs
  }

  static func += (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 + rhs
    }
  }

  static func -= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 - rhs
    }
  }

  static func *= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 * rhs
    }
  }

  static func /= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 / rhs
    }
  }
}

public extension Atomic where Value.ValueType == Float {
  static func == (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value == rhs
  }

  static func < (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value < rhs
  }
  
  static func > (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value > rhs
  }
  
  static func <= (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value <= rhs
  }

  static func >= (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value >= rhs
  }
  
  static func + (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value + rhs
  }

  static func += (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 + rhs
    }
  }

  static func -= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 - rhs
    }
  }

  static func *= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 * rhs
    }
  }

  static func /= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 / rhs
    }
  }
}


public extension Atomic where Value.ValueType == Double {
  static func == (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value == rhs
  }

  static func < (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value < rhs
  }
  
  static func > (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value > rhs
  }
  
  static func <= (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value <= rhs
  }

  static func >= (lhs: Atomic, rhs: Value.ValueType) -> Bool {
    return lhs.value >= rhs
  }
  
  static func + (lhs: Atomic, rhs: Value.ValueType) -> Value.ValueType {
    return lhs.value + rhs
  }

  static func += (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 + rhs
    }
  }

  static func -= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 - rhs
    }
  }

  static func *= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 * rhs
    }
  }

  static func /= (lhs: inout Atomic, rhs: Value.ValueType) {
    lhs.mutate {
      return $0 / rhs
    }
  }
}
