//
//  AtomicCollection.swift
//  Atomics
//
//  Created by Yaniv Hasbani on 31/03/2023.
//  Copyright © 2023 Yaniv Hasbani. All rights reserved.
//

import Foundation

public protocol AtomicCollection {
  var count: Int { get }
  var isEmpty: Bool { get }
  
  mutating func removeAll(keepingCapacity: Bool)
}

public extension Atomic where Value.ValueType: AtomicCollection {
  var count: Int {
    return self.value.count
  }
  
  var isEmtpy: Bool {
    return self.value.isEmpty
  }
  
  func removeAll(_ keepingCapacity: Bool = false) {
    self.mutate { wrappedValue in
      wrappedValue.removeAll(keepingCapacity: keepingCapacity)
      
      return wrappedValue
    }
  }
}
