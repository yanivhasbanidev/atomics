# Atomics

A GCD based effort to create atomicity, for variables (much like Objective-C), on different types.

# Usage


Currently, we support the following types:

* [AtomicArray](#atomicarray)
* [AtomicDictionary](#atomicdictionary)

# AtomicArray

### Initialization

```swift

// This will initialize an atomic empty array
var atomicArray = Atomic<Array<MyType>>()

```

### General usage
```swift
// Add element
atomicArray.append(element)

// Remove element in the 4th position
atomicArray.remove(at:4)
```

### For loop

```swift
for element in atomicArray.value {

}
```

# AtomicDictionary

### Initialization
```swift

// This will initialize an atomic empty dictionary
var atomicDictionary = Atomic<Dictionary<Key, Value>>()

```

### General usage
```swift
// add or update value for key
atomicDictionary.updateValue(value, key)

// remove
atomicDictionary.removeValue(forKey: key)
```

# Abstract

When it comes to achiving atomicity, we need to be able to control the synchronization of different getters/setters of a property.

We try to achive that by using GCD in the following manner:

In a world we only had **get** requests, atomicity is trivial since the state never changes.
So, our problems reside when a **set** action occurs.

If we will be able to differentiate a **set** action, confine it to run sepertly, and than allow all following **get** requests, we will be able to achive atomicity.

We do that using **GCD**

## Synchronization

So, how can we get a seperate queue that behaves like a **concurrent queue for get requests** and **serial queue for set requests?

**barriers**!

from apple's documentations:([barrier](https://developer.apple.com/documentation/dispatch/dispatch_barrier))

```
Use a barrier to synchronize the execution of one or more tasks in your dispatch queue. 
When you add a barrier to a concurrent dispatch queue, the queue delays the execution of the barrier block
(and any tasks submitted after the barrier) until all previously submitted tasks finish executing.
After the previous tasks finish executing, the queue executes the barrier block by itself.
Once the barrier block finishes, the queue resumes its normal execution behavior.
```

So, in our implementation we will use the following to achive just that:

```swift
let queue = DispatchQueue(label: "com.skektech.atomic.\(UUID())",
                          attributes: .concurrent,
                          target: atomicityQueueTarget)
```

* notice the **target: atomicityQueueTarget** set to be able to control the amount of actual threads we create for each queue.

From Apple's documentation:([setTarget](https://developer.apple.com/documentation/dispatch/dispatchobject/1452989-settarget))
```
Use target queues to redirect work from several different queues onto a single queue. 
You might do this to minimize the total number of threads your app uses,
while still preserving the execution semantics you need. 
The system doesn't allocate threads to the dispatch queue if it has a target queue,
unless that target queue is a global concurrent queue.
```

Now that we have a good synchronization mechanism doing what we wanted, lets see how we use it.

## Getters

In order to be able to support a fast atomic getter, we need to be able to differentiate a state in which a mutation might occur (A.K.A: set).
To do so, we created the following:

```swift
  // Private value to keep the actually wrapped value to be atomized
  private var _value: Value.ValueType

  // A public getter for getting the current value
  public var value: Value.ValueType {
    get {
      var value = Value.defaultValue
      self.queue.sync {
        value = self._value
      }
      
      return value
    }
  }
```

As you can see, users can get the current value simply by calling `.value`.
The getter for that will call the synchronization queue exaplained earlier in a synchronous fashion (although this is a concurrent queue and so enabling multiple reads at the same time)

## Setters (Mutation)

To create a situation in which when setting a new value to `_value`, we will assure no other operation occurs, we will a `mutate` function that will receive a block from the user.
That block will run on the designated queue, hence enabling us to control that only one set at a time occurs. and no other get requests are happening at the same time.

